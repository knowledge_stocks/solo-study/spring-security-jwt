package com.kakaruu.example.security.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kakaruu.example.security.entity.User;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoDTO {

    @NotNull
    @Size(min = 3, max = 50)
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull
    @Size(min = 3, max = 100)
    private String pwd;

    @NotNull
    @Size(min = 3, max = 50)
    private String nickname;

    public static UserInfoDTO fromEntity(User entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .username(entity.getUsername())
                .pwd(entity.getPwd())
                .nickname(entity.getNickname())
                .build();
    }
}

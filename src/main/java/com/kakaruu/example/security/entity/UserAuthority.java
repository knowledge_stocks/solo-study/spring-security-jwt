package com.kakaruu.example.security.entity;

import com.kakaruu.example.security.entity.idclass.UserAuthorityKey;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@IdClass(UserAuthorityKey.class)
public class UserAuthority {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", columnDefinition = "INT UNSIGNED")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @Id
    // 기본키 컬럼 하나밖에 없지만 Eager로 설정되어 있으면 따로 select 쿼리가 발생되므로 LAZY로 설정
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "authority", columnDefinition = "VARCHAR(50)")
    private Authority authority;
}

package com.kakaruu.example.security.entity.idclass;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAuthorityKey implements Serializable {
    private Long user;
    private String authority;
}

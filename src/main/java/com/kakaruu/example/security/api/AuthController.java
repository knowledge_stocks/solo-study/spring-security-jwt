package com.kakaruu.example.security.api;

import com.kakaruu.example.security.dto.LoginDTO;
import com.kakaruu.example.security.dto.TokenDTO;
import com.kakaruu.example.security.jwt.JwtFilter;
import com.kakaruu.example.security.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AuthController {

    private final JwtTokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    @PostMapping("/login")
    public ResponseEntity<TokenDTO> login(@Valid @RequestBody LoginDTO loginDTO) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPwd());

        // dto에 입력된 정보로 사용자 인증 처리
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication); // 이부분 필요 없는거 아닌가??

        String jwt = tokenProvider.createToken(authentication);

        HttpHeaders headers = new HttpHeaders();
        headers.add(JwtFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

        return new ResponseEntity(new TokenDTO(jwt), headers, HttpStatus.OK);
    }

    @GetMapping("/auth/test")
    public ResponseEntity test(Authentication authentication) {
        return new ResponseEntity(authentication, HttpStatus.OK);
    }

    @GetMapping("/seller/test")
    public ResponseEntity sellerTest(Authentication authentication) {
        return new ResponseEntity(authentication, HttpStatus.OK);
    }

    @GetMapping("/admin/test")
    public ResponseEntity adminTest(Authentication authentication) {
        return new ResponseEntity(authentication, HttpStatus.OK);
    }
}

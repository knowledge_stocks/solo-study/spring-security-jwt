package com.kakaruu.example.security.jwt;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
// Filter를 아무 생각없이 Bean으로 설정하면 안된다.
// Filter 체인의 순서를 생각하고 Order를 잘 설정해주어야 한다.
// 그냥 @Component로 설정하면 UsernamePasswordAuthenticationFilter 보다 뒤에 체인되기 때문에 인증 처리가 제대로 되지 않는다.
// 어노테이션으로는 Filter Order를 설정하기 애매한 것 같다.
// FilterRegistrationBean을 사용하거나, 그냥 SecurityConfig에서 addFilterBefore로 수동 등록 시키자.
//
// 참고자료 1. FilterRegistrationBean으로 필터 순서와, Url패턴 설정하는 방법. 복잡한 필터 설정시 사용하면 좋을듯하다.
// https://jronin.tistory.com/124
//
// 참고자료 2. Bean으로 등록해서 자동으로 등록된 Filter를 비활성화하고, addFilterBefore로 원하는 순서에 배치하는 방법
// https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=kpetera&logNo=220775387604
//@Component
@RequiredArgsConstructor
public class JwtFilter extends GenericFilterBean {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private final JwtTokenProvider tokenProvider;

    @Override
    public void doFilter(
            ServletRequest servletRequest,
            ServletResponse servletResponse,
            FilterChain filterChain
    ) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String token = resolveToken(httpServletRequest); // header에서 token 추출
        String requestURI = httpServletRequest.getRequestURI();

        // 토큰 유효성 확인
        if (tokenProvider.validateToken(token)) {
            // 토큰의 정보로 Authentication 설정
            Authentication authentication = tokenProvider.getAuthentication(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            log.debug("Security Context에 '{}' 인증 정보를 저장했습니다, uri: {}", authentication.getName(), requestURI);
        } else {
            log.debug("유효한 JWT 토큰이 없습니다, uri: {}", requestURI);
        }

        // 다음 필터로 전이
        filterChain.doFilter(servletRequest, servletResponse);
    }

    // 헤더에 설정되어있는 토큰을 가져온다.
    private String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);

        // 유효성 확인
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            // 토큰 앞에 붙어있는 Bearer 제거
            return bearerToken.substring("Bearer ".length());
        }
        return null;
    }
}

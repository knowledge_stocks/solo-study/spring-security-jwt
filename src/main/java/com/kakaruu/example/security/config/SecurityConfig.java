package com.kakaruu.example.security.config;

import com.kakaruu.example.security.jwt.JwtAccessDeniedHandler;
import com.kakaruu.example.security.jwt.JwtAuthenticationEntryPoint;
import com.kakaruu.example.security.jwt.JwtSecurityConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
@RequiredArgsConstructor
/*EnableGlobalMethodSecurity
메소드를 호출하기 전에 권한을 확인할 수 있도록 하는 설정
메소드에 @Secured/@RollAllowed 나 @PreAuthorize/@PostAuthorize 를 사용해서 사용자 권한에 따라 메소드 호출을 제어할 수 있다.
*/
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthenticationEntryPoint authenticationEntryPoint;
    private final JwtAccessDeniedHandler accessDeniedHandler;
    private final JwtSecurityConfig jwtSecurityConfig;

    @Override
    public void configure(WebSecurity web) {
        web
                // h2-console의 경로들은 security 관리에서 제외한다.
                // h2-console은 jsessionid로 로그인 인증을 따로 관리하기 때문
                .ignoring()
                .antMatchers(
                        "/h2-console/**"
                        // ,"/favicon.ico" // 예제에서는 favicon.ico도 제외 시키는데 왜인지 모르겠다. 애초에 사용하고 있지 않은 경로이므로 걍 주석처리
                );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .cors()

                // 인증 에러 처리 설정
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .accessDeniedHandler(accessDeniedHandler)

                // 요건 아직 무슨 설정인지 확실히 모르겠다.
                // h2-console을 위한 설정이라는데 일단 주석처리
//                .and()
//                .headers()
//                .frameOptions()
//                .sameOrigin()

                // JWT를 사용할 것이므로 session 설정을 Stateless로 변경.
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                // 경로 권한 설정 /auth, /api/auth로 시작하는 경로는 인증 필요
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/**", "/auth/**").authenticated()
                .antMatchers("/api/admin/**", "/admin/**").hasRole("ADMIN")
                .antMatchers("/api/seller/**", "/seller/**").hasRole("SELLER")
                .anyRequest().permitAll()

                // JwtSecurityConfig을 적용
//                .and()
//                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
                .and()
                .apply(jwtSecurityConfig);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedOrigin("*");
        configuration.addAllowedMethod("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
